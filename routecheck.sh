#!/bin/bash
fromAccount=$1
toAccount=$2
mobileNumber=$3
timeout=$4
authorisation=$5

datetime=$(date '+%d/%m/%Y %H:%M:%S')
dateepoch=$(date '+%s')
body="Heartbeat $dateepoch from $fromAccount to $toAccount at $datetime"

messagedispatcherresponse=$(curl --silent -H "Authorization: Basic $authorisation" -H "Content-type: application/xml" -X POST -d "<?xml version='1.0' encoding='UTF-8'?><messages><accountreference>$fromAccount</accountreference><message><to>$mobileNumber</to><body>$body</body></message></messages>" --max-time $timeout https://api.esendex.com/v1.0/messagedispatcher)

messageid=$(echo $messagedispatcherresponse | xmlstarlet sel -N x="http://api.esendex.com/ns/" -t -m '//x:messageheaders/x:messageheader/@id' -v . -n)

messagestatusresponse=""
messagestatus="Submitted"
timer=0
checkingoutbound=true
success=false

while [ "$checkingoutbound" = true ]; do
        let timer=timer+1
        if [ "$timer" -gt "$timeout" ]; then
                let checkingoutbound=false
        fi

        messagestatusresponse=$(curl --silent -H "Authorization: Basic $authorisation" https://api.esendex.com/v1.0/messageheaders/$messageid)
        messagestatus=$(echo $messagestatusresponse | xmlstarlet sel -N x="http://api.esendex.com/ns/" -t -m '//x:messageheader/x:status' -v . -n)

        if [ $messagestatus == "Delivered" ]; then
                let success=true
                let checkingoutbound=false
        fi

        sleep 1
done

checkinginbound=true
successinbound=false
inboundtimer=0

while [ "$checkinginbound" = true ]; do
        let inboundtimer=inboundtimer+1
        if [ "$inboundtimer" -gt "$timeout" ]; then
                let checkinginbound=false
        fi

        inboxstatusresponse=$(curl --silent -H "Authorization: Basic $authorisation" https://api.esendex.com/v1.0/inbox/$toAccount/messages)
        inboxstatus=$(echo $inboxstatusresponse | xmlstarlet sel -N x="http://api.esendex.com/ns/" -t -m '//x:messageheaders/x:messageheader/x:summary' -v . -n)

        for inboxmessage in $inboxstatus; do
                if [ $inboxmessage = $dateepoch ]; then
                        let successinbound=true
                        let checkinginbound=false
                fi
        done
done

testoutputmessage="Test from $fromAccount to $toAccount via $mobileNumber"
amsmessagedetaillink="https://ams.esendex.com/MessageReporting/MessageDetail/$messageid"
inboundmessagedetaillink="https://ams.esendex.com/MessageReporting?AccountReference=$toAccount&Direction=Received"

ECHO="/bin/echo"

if [ "$success" = 0 ] && [ "$successinbound" = 0 ]; then
        message="OK : $testoutputmessage took $timer seconds : <a href=\"${amsmessagedetaillink}\">ams.esendex.com</a>"
        $ECHO $message
        exit 0
elif [ "$success" = 0 ] && [ "$successinbound" = 1]; then
        message="CRITICAL: $testoutputmessage did not arrive in the Inbox of $toAccount after $timeout : <a href=\"${inboundmessagedetaillink}\">ams.esendex.com</a>"
        $ECHO $message
        exit 2
else
        message="CRITICAL: $testoutputmessage timed out after $timeout : <a href=\"${amsmessagedetaillink}\">ams.esendex.com</a>"
        $ECHO $message
        exit 2
fi